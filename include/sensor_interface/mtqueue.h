
#include <condition_variable>
#include <deque>
#include <mutex>

template <typename DataType>
class MTQueue {
 public:
  MTQueue() {}

  MTQueue(const MTQueue<DataType>& rhs) {
    std::unique_lock lck(rhs.m_data_mutex);
    m_data = rhs.m_data;
  }

  MTQueue& operator=(const MTQueue<DataType>& rhs) {
    if (&rhs != this) {
      std::scoped_lock lck(m_data_mutex, rhs.m_data_mutex);

      m_data = rhs.m_data;
    }
    return *this;
  }

  std::deque<DataType> popAll() {
    // clear previous data
    // m_data_ret is NOT thread-safe -> only one thread can call this func!
    m_data_ret.clear();

    // get data then unlock
    std::unique_lock lck(m_data_mutex);
    // m_cv.wait(lck, [this] { return !m_data.empty();});
    m_data.swap(m_data_ret);
    lck.unlock();

    return m_data_ret;
  }

  template <typename ElemType>
  void push(ElemType&& elem) {
    std::unique_lock lck(m_data_mutex);
    m_data.push_back(std::forward<ElemType>(elem));
    // m_cv.notify_one();
  }

  size_t getSize() const {
    std::unique_lock lck(m_data_mutex);
    return m_data.size();
  }

  void clear() {
    std::unique_lock lck(m_data_mutex);
    m_data.clear();
    m_data_ret.clear();
  }

 private:
  // std::condition_variable m_cv;
  mutable std::mutex m_data_mutex;

  std::deque<DataType> m_data;
  std::deque<DataType> m_data_ret;

};  // class Buffer
