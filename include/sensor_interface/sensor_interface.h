
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <rosbag/bag.h>
#include <sensor_interface/mtqueue.h>
#include <std_srvs/Empty.h>
#include <topic_tools/shape_shifter.h>

#include <memory>
#include <mutex>
#include <thread>
#include <unordered_map>
#include <vector>

namespace sensor_interface {

struct MsgRaw
{
  topic_tools::ShapeShifter::ConstPtr msg;
  ros::Time time_received;
};


class SensorInterface : public nodelet::Nodelet {
 public:
  using DataQueue = MTQueue<MsgRaw>;
  using TopicDataQueueMap = std::unordered_map<std::string, DataQueue>;
  using BagWriterThreadVec = std::vector<std::thread>;

 public:
  void onInit() override;

  ~SensorInterface() override;

 private:
  void sensorCallback(
      const ros::MessageEvent<const topic_tools::ShapeShifter>& event);

  bool startRecordCallback(std_srvs::Empty::Request& req,
                           std_srvs::Empty::Response& res);
  bool stopRecordCallback(std_srvs::Empty::Request& req,
                          std_srvs::Empty::Response& res);

 private:
  void writer(const std::string&);
  void createThread();
  void removeThread();

  std::shared_ptr<rosbag::Bag> m_bag_;
  mutable std::mutex m_bag_mutex_;
  std::condition_variable m_cv;

  std::string m_rosbag_path_;
  std::string m_rosbag_prefix_;
  std::vector<std::string> m_rosbag_record_topics_;
  std::vector<ros::Subscriber> m_subscriber_vec_;

  TopicDataQueueMap m_tqm_;
  BagWriterThreadVec m_bag_thread_;

  std::atomic_bool m_is_recording_;
  ros::ServiceServer m_start_record_srv_;
  ros::ServiceServer m_stop_record_srv_;

};  // class SensorInterface

}  // namespace sensor_interface
