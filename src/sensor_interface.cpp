
#include <pluginlib/class_list_macros.h>
#include <sensor_interface/sensor_interface.h>

#include <ctime>
#include <iomanip>
#include <iostream>

namespace sensor_interface {

void SensorInterface::onInit() {
  NODELET_INFO("Initializing SensorInterface");

  ros::NodeHandle &private_nh = getMTPrivateNodeHandle();

  // retrieve the path and topics
  private_nh.getParam("/rosbag_path", m_rosbag_path_);
  private_nh.getParam("/rosbag_record_topics", m_rosbag_record_topics_);
  private_nh.getParam("/rosbag_prefix", m_rosbag_prefix_);

  NODELET_INFO_STREAM("Get Rosbag File Path: " << m_rosbag_path_);
  NODELET_INFO_STREAM("Get Rosbag File Prefix: " << m_rosbag_prefix_);
  NODELET_INFO_STREAM(
      "Get Num of Rosbag topics: " << m_rosbag_record_topics_.size());

  m_start_record_srv_ = private_nh.advertiseService(
      "start_recording", &SensorInterface::startRecordCallback, this);
  m_stop_record_srv_ = private_nh.advertiseService(
      "stop_recording", &SensorInterface::stopRecordCallback, this);

  for (auto & m_rosbag_record_topic : m_rosbag_record_topics_) {
    // setup Subscriber
    ros::Subscriber sub =
        private_nh.subscribe(m_rosbag_record_topic, 2000,
                             &SensorInterface::sensorCallback, this);
    m_subscriber_vec_.push_back(sub);
    // setup dataqueue
    m_tqm_.insert(std::make_pair(m_rosbag_record_topic, DataQueue()));
  }

  NODELET_INFO_STREAM("Init SensorInterface done!");
}

SensorInterface::~SensorInterface() {
  NODELET_INFO_STREAM("Shutdown SensorInterface Begin!");

  removeThread();

  if (m_bag_ && m_bag_->isOpen()) {
    m_bag_->close();
  }
  NODELET_INFO_STREAM("Closed Rosbag File: " << m_rosbag_path_);
}

bool SensorInterface::startRecordCallback(std_srvs::Empty::Request &req,
                                          std_srvs::Empty::Response &res) {
  if (m_is_recording_) {
    NODELET_ERROR_STREAM("Already recording, please stop first!");
    return false;
  }

  // brand new start
  for (auto &p : m_tqm_) {
    p.second.clear();
  }

  // get the time for bag file
  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);
  std::ostringstream oss;
  oss << std::put_time(&tm, "%Y%m%d_%H%M%S");
  const auto datetimestr = oss.str();
  const std::string bagname = datetimestr + '_' + m_rosbag_prefix_;
  const std::string bagpath = m_rosbag_path_ + '/' + bagname;

  NODELET_INFO_STREAM("Target Bag File Path: " << bagpath);

  // open bag file
  if (m_bag_) {
    m_bag_ = nullptr;
  }
  m_bag_ = std::make_shared<rosbag::Bag>();
  m_bag_->open(bagpath, rosbag::bagmode::Write);
  if (m_bag_->isOpen()) {
    NODELET_INFO_STREAM("Start recording data in the bag: " << bagpath);
    m_is_recording_ = true;

    // start the writing threads
    createThread();

    return true;
  } else {
    NODELET_ERROR_STREAM("Open Bag File FAILED!");
    return false;
  }
}

bool SensorInterface::stopRecordCallback(std_srvs::Empty::Request &req,
                                         std_srvs::Empty::Response &res) {
  NODELET_INFO_STREAM("Stop Recording!");
  if (!m_is_recording_) {
    NODELET_ERROR_STREAM("Not recording, please start first!");
    return false;
  }

  m_is_recording_ = false;

  removeThread();

  if (m_bag_ && m_bag_->isOpen()) {
    m_bag_->close();
  }
  return true;
}

void SensorInterface::sensorCallback(
    const ros::MessageEvent<const topic_tools::ShapeShifter> &event) {
  ros::M_string &header = event.getConnectionHeader();
 // topic_tools::ShapeShifter::ConstPtr msg = event.getMessage();
  auto &q = m_tqm_.at(header["topic"]);

  MsgRaw raw;
  raw.msg = event.getMessage();
  raw.time_received = event.getReceiptTime();

  q.push(raw);
}

void SensorInterface::writer(const std::string &topic) {
  while (ros::ok()) {
    if (!m_bag_ || !m_bag_->isOpen()) {
      NODELET_INFO_STREAM("Bag file Closed! " << topic << " Exit!");
      break;
    }

    if (!m_is_recording_) {
      NODELET_INFO_STREAM("Recording Stop! " << topic << " Exit!");
      break;
    }

    // pop the data
    auto &q = m_tqm_.at(topic);
    auto data = q.popAll();

    // write data into bag
    std::unique_lock lck(m_bag_mutex_);
    for (auto &raw : data) {
      m_bag_->write(topic, raw.time_received, raw.msg);
    }
  }
}

void SensorInterface::createThread() {
  for (auto & m_rosbag_record_topic : m_rosbag_record_topics_) {
    // start the threads
    m_bag_thread_.emplace_back(std::thread(
        &SensorInterface::writer, this, std::ref(m_rosbag_record_topic)));
  }
  NODELET_INFO_STREAM("Start " << m_rosbag_record_topics_.size()
                               << " thread(s) for writing!");
}

void SensorInterface::removeThread() {
  for (auto &t : m_bag_thread_) {
    t.join();
  }
  m_bag_thread_.clear();
  NODELET_INFO_STREAM("Remove " << m_rosbag_record_topics_.size()
                                << " thread(s) for writing!");
}

}  // namespace sensor_interface
   //
PLUGINLIB_EXPORT_CLASS(sensor_interface::SensorInterface, nodelet::Nodelet);
